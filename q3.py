import json
import math
import sys
import bpy


filepath = "sphere.obj"



mesh = bpy.ops.mesh.primitive_uv_sphere_add()
ob = bpy.context.object
me = ob.data
ob.name = 'SPHEREOBJ'
me.name = 'SPHEREMESH'

# test pour changer la couleur de la sphere en rouge
activeObject = bpy.context.active_object 
mat = bpy.data.materials.new(name="MonMaterial")
activeObject.data.materials.append(mat) 
bpy.context.object.active_material.diffuse_color = (1, 0, 0) 

