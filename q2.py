import json
import math
# import bpy utilisable seulement dans un interpreteur blender
import sys

# python q2.py exemples/scene.json
def main():
	# je veux l'argument a la position 1 dans mon tableau
	print(sys.argv[1]) 
	sceneInfo = loadJson(sys.argv[1])
	createScene(sceneInfo)



def loadJson(jsonPath):
	
	data = json.load(open(jsonPath))

	# print le json de facons plus lisible
	#print json.dumps(data, indent=4, sort_keys=True)
	return data

def createScene(sceneInfo):
	objectArray = sceneInfo['objects']
	object1 = objectArray[0]
	object2 = objectArray[1]
	object3 = objectArray[2]
	print "Scene of dimensions" ,sceneInfo['width'], "X" ,sceneInfo['height']
	print "\n Containing", len(sceneInfo['objects']), "objects:\n"
	print "- A ", object1['type'], " of radius", object1['radius'], "centered in (", object1['center'],")"
	print "- A ", object2['type'], " of radius", object2['radius'], "centered in (", object2['center'],")"
	print "- A ", object3['type'], " of width", object3['width'],"and height", object3['height'],"centered in (", object3['center'],")\n"
	
main()
    
