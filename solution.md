# Solution au devoir 1

## Auteur

Olivier Petit PETO19049107

Utilisateur de python 2.7


## Solution à la question 1

les tests dans le doctest on été modifier comme suit:
 p = Point3D(4,-2,1)

 au lieu de

  p = Point3D(4, -2, 1)

Commande pour exécuter: python -m doctest q1.py

## Solution à la question 2

2.1 : la sortie fonction, avec le path : python q2.py exemples/scene.json
2.2 : Pillow ne veut pas s'installer sur ma machine à cause de pip.
2.3 : non complété


## Solution à la question 3

Je peux créer un sphere avec un script python, mais il doit être lancer une fois a l'intérieur de blender, car le import bpy n'est pas visible si il
n'est pas dans blender.



## Solution à la question 4
Non fait.
Non complété

## Dépendances

Indiquez ici toutes les dépendances pour faire fonctionner votre projet. Une
dépendance est un logiciel ou une bibliothèque qui doit être installée pour
pouvoir reproduire les commandes et les résultats que vous obtenez.

## Références

import bpy
import json
import math
https://blender.stackexchange.com/questions/93298/create-a-uv-sphere-object-in-blender-from-python?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

## État du devoir
Le devoir n'est pas complété.
Q1: Fait
Q2: 2.1 fait, mais 2.2 ne fonctionne pas dû à Pillow, 2.3 non fait
Q3: J'ai un début de fichier qui génère une sphere dans blender, mais n'est pas terminé 
Q4: Non fait

Indiquez ici si le devoir est complété, ou s'il y a certaines questions pour
lesquelles aucune réponse n'est fournie (ou simplement une réponse partielle).









